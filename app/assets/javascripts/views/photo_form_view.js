function PhotoFormView() {
  this.$el = $("<div></div>");
};

PhotoFormView.prototype.render = function () {
  this.$el.html(JST["photo_form"]);

  var that = this;
  this.$el.on('submit', "form", function(event){
      event.preventDefault();
      that.submit(event);
  });

  return this;
};

PhotoFormView.prototype.submit = function (event) {

  var formData = $(event.currentTarget).serializeJSON();
  console.log(formData.photo);
  var photo = new PT.Photo(formData.photo);
  console.log(photo);
  photo.create();


  // var photo = new PT.Photo(formData);
//   photo.create();

};
