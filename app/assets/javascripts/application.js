// This is a manifest file that'll be compiled into application.js,
// which will include all the files listed below.
//
// Any JavaScript/Coffee file within this directory,
// lib/assets/javascripts, vendor/assets/javascripts, or
// vendor/assets/javascripts of plugins, if any, can be referenced
// here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll
// appear at the bottom of the the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE
// PROCESSED, ANY BLANK LINE SHOULD GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery.serializeJSON
//= require underscore
//
//= require_tree ./models
//= require_tree ./views
//= require_tree ../templates
//
//= require_tree .

(function(root) {

  var PT = root.PT = (root.PT || {});

  PT.initialize = function (id) {

    Photo.fetchByUserId(id, function(photos) {
      var plv = new PhotosListView();
      $('#content').append(plv.render(photos));
    });

    var form = new PhotoFormView();
    $('#form').append(form.render().$el);

  };

  var Photo = PT.Photo = function (obj) {
    this.attr = obj;
  };

  Photo.all = [];

  _.extend(Photo.prototype, {
    get: function(attr_name) {
      return this.attr[attr_name];
    },

    set: function(attr_name, val) {
      return this.attr[attr_name] = val;
    },

    create: function(callback) {
      var that = this;

      $.ajax({
        url: "api/photos",
        type: "POST",
        data: { photo: this.attr },
        dataType: "json",
        success: function(data){

          _.extend(that.attr, data);
          Photo.all.push(that);
          // callback(that);
        }
      });
    },

    update: function() {
      var that = this;

      $.ajax({
        url: "api/photos/" + that.attr.id,
        type: "PUT",
        data: { photo: { title: this.attr.title, url: this.attr.url } },
        dataType: "json",
        success: function(data){
          _.extend(that.attr, data);
        }
      });
    },

    save: function() {
      if (this.get("id") == null) {
        this.create();
      } else {
        this.update();
      }
    }

  });

  Photo.fetchByUserId = function (userId, callback) {
    var that = this;

    $.ajax({
      url: "api/users/" + userId + "/photos",
      type: "GET",
      dataType: "json",
      success: function(data){

        data.forEach(function(pojo) {
          pojo = new Photo(pojo);
          Photo.all.push(pojo);
        })

        callback(Photo.all);
      }

    });
  }

})(this);

